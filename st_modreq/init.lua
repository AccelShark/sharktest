---------------------------------------
-- HEADER
---------------------------------------
sharktest.print("Entering st_modreq")
sharktest.modreq = {}
sharktest.modreq.requests = {}

---------------------------------------
-- Privileges
---------------------------------------
minetest.register_privilege("st_modreq", {
    description = "Gives the player the ability to handle admin requests",
    give_to_admin = true,
})

sharktest.modreq.send = function(what)
    for _, player in ipairs(minetest.get_connected_players()) do
        local name = player:get_player_name()
        local bool, missing_privs = minetest.check_player_privs(name, {
            st_modreq = true
        })
        if bool then 
            sharktest.chat_send_player(name, "(st_modreq) " .. what)
        end
    end
end

minetest.register_on_joinplayer(function(player)
    local name = player:get_player_name()
    local bool, missing_privs = minetest.check_player_privs(name, {
        st_modreq = true
    })
    if bool then 
        sharktest.chat_send_player(name, "(st_modreq) You have " .. #sharktest.modreq.requests .. " requests open.")
    end
end) 
---------------------------------------
-- Commands
---------------------------------------
sharktest.modreq.cmd_modreq = {
    params = "<what>",
    description = "Requests admins' attention for <what>",
    privs = {},
    func = function(name, param)
        if param == "" or param == nil then
            return false, "Please don't send blank admin requests"
        end
        local player = minetest.get_player_by_name(name)
        local pos = nil
        if player ~= nil then
            pos = player:get_pos()
        end
        table.insert(sharktest.modreq.requests, {name=name, pos=pos, request=param})
        sharktest.modreq.send("Request " ..  #sharktest.modreq.requests .. " filed by " .. name .. " in pos " .. minetest.pos_to_string(pos) .. ": " .. param)
        return true, "Request " .. #sharktest.modreq.requests .. " filed"
    end
}
minetest.register_chatcommand("modreq", sharktest.modreq.cmd_modreq)

sharktest.modreq.cmd_mview = {
    params = "<index>",
    description = "Views an admin request",
    privs = { st_modreq = true },
    func = function(name, param)
        local index = tonumber(param)
        if index == nil or index > #sharktest.modreq.requests then
            return false, "Please specify a valid index"
        end
        return true, "Request " .. index .. " was filed by " ..  sharktest.modreq.requests[index].name .. " in pos " .. minetest.pos_to_string(sharktest.modreq.requests[index].pos) .. ": " .. param
    end
}
minetest.register_chatcommand("modreq", sharktest.modreq.cmd_modreq)

sharktest.modreq.cmd_mclose = {
    params = "<index> [reason]",
    description = "Closes request <index> for an optional [reason]",
    privs = { st_modreq = true },
    func = function(name, param)
        local rindex, reason = string.match(param, "(%d+)(.*)")
        local index = tonumber(rindex)
        if index == nil then
            return false, "Please specify a valid index"
        end
        if reason == "" then        
            sharktest.chat_send_player(sharktest.modreq.requests[index].name, "Your request " .. index .. " has been closed for no reason")
            sharktest.modreq.send("Request " ..  index .. " closed by " .. name .. " for no reason")
        else
            sharktest.chat_send_player(sharktest.modreq.requests[index].name, "Your request " .. index .. " has been closed: " .. reason)
            sharktest.modreq.send("Request " ..  index .. " closed by " .. name .. ": " .. reason)
        end
        table.remove(sharktest.modreq.requests, index)
        return true, "Request " .. index .. " closed"
    end
}
minetest.register_chatcommand("mclose", sharktest.modreq.cmd_mclose)

---------------------------------------
-- FOOTER
---------------------------------------
sharktest.print("Exiting st_modreq")