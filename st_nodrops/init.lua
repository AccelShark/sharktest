---------------------------------------
-- HEADER
---------------------------------------
sharktest.print("Entering st_nodrops")

sharktest.nodrops = {}
sharktest.nodrops.old_drop = minetest.item_drop
minetest.item_drop = function(itemstack, dropper, pos)
    itemstack:clear()
    return itemstack
end

---------------------------------------
-- FOOTER
---------------------------------------
sharktest.print("Exiting st_nodrops")