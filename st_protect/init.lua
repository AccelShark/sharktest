---------------------------------------
-- HEADER
---------------------------------------
sharktest.print("Entering st_protect")
sharktest.protect = {}
sharktest.protect.prot_cache = {}
sharktest.protect.user_cache = {}
local mod_storage = minetest.get_mod_storage()

sharktest.protect.load = function()
    sharktest.print("Protection system loading")
    local pamount = mod_storage:get_int("protINDEX_0")
    local uamount = mod_storage:get_int("userINDEX_0")
    for i = 1, pamount do
        local prot = {}
        prot.score = mod_storage:get_int("protSCORE_" .. i)
        prot.owner = mod_storage:get_string("protOWNER_" .. i)
        prot.pmin = minetest.string_to_pos(mod_storage:get_string("protVPMIN_" .. i))
        prot.pmax = minetest.string_to_pos(mod_storage:get_string("protVPMAX_" .. i))
        table.insert(sharktest.protect.prot_cache, prot)
    end
    for i = 1, uamount do
        local score = mod_storage:get_int("userSCORE_" .. i)
        local uname = mod_storage:get_string("userUNAME_" .. i)
        sharktest.protect.user_cache[uname] = score
    end
    sharktest.print("Protection system loaded")
end

sharktest.protect.save = function()
    -- I choose to use a strong database, since weak ones may poison the data.
    -- The database should not be sparse. It should be dense for easy traversal.
    sharktest.print("Protection system saving")

    mod_storage:set_int("protINDEX_0", #sharktest.protect.prot_cache)

    for i, n in ipairs(sharktest.protect.prot_cache) do
        local vpmin = minetest.pos_to_string(n.pmin)
        local vpmax = minetest.pos_to_string(n.pmax)
        mod_storage:set_int("protSCORE_"..i, n.score)
        mod_storage:set_string("protOWNER_"..i, n.owner)
        mod_storage:set_string("protVPMIN_"..i, minetest.pos_to_string(n.pmin))
        mod_storage:set_string("protVPMAX_"..i, minetest.pos_to_string(n.pmax))
    end

    local i = 0
    for k, v in pairs(sharktest.protect.user_cache) do
        i = i + 1
        mod_storage:set_int("userSCORE_" .. i, v)
        mod_storage:set_string("userUNAME_" .. i, k)
    end

    mod_storage:set_int("userINDEX_0", i)
    
    sharktest.print("Protection system saved")
end

---------------------------------------
-- Privileges
---------------------------------------
minetest.register_privilege("st_protect", {
    description = "Grant the ability to create a protection up to the player's pscore",
    give_to_admin = false,
})

---------------------------------------
-- Callbacks
---------------------------------------
sharktest.protect.old_callback = minetest.is_protected
sharktest.protect.is_protected = function(pos, name)
    local bool, missing_privs = minetest.check_player_privs(name, {
        protection_bypass = true
    })

    if bool then return false end

    local pscore = 0x0000
    local pthis = sharktest.protect.user_cache[name]
    if pthis == nil then
        pthis = 0x0000
    end
    local ipos = 0
    local owner = nil
    for i, n in ipairs(sharktest.protect.prot_cache) do
        local area = VoxelArea:new{MinEdge=n.pmin, MaxEdge=n.pmax}
        if area:containsp(pos) and n.score > pscore then
            pscore = n.score
            owner = n.owner
            ipos = i
        end
    end
    if pscore > pthis then
        sharktest.print("Player " .. name .. " tried to modify a protected node {ipos=" .. ipos .. ", pthis=" .. pthis .. ", pscore=" .. pscore .. "}")
        sharktest.chat_send_player(name, "You are not allowed to modify this node")
        return true
    elseif owner ~= nil then
        return name ~= owner 
    end

    return sharktest.protect.old_callback(pos, name)
end
minetest.is_protected = sharktest.protect.is_protected

sharktest.protect.is_area_protected = function(pmin, pmax, name, pscore)
    for i, n in ipairs(sharktest.protect.prot_cache) do
        local area = VoxelArea:new{MinEdge=n.pmin, MaxEdge=n.pmax}
        for j in area:iterp(pmin, pmax) do
            if area:containsi(j) and n.pscore > pscore then
                return true
            end
        end
    end
    
    return minetest.is_area_protected(pmin, pmax, name)
end

---------------------------------------
-- Commands
---------------------------------------
sharktest.protect.cmd_protect = {
    params = "<score> <pmin> <pmax>",
    description = "Protect area selected by <pmin> and <pmax>",
    privs = { st_protect = true },
    func = function(name, param)
        local rwanted, area = string.match(param, "(%x+) (.+)")
        local pwanted = tonumber(rwanted, 16)
        local pmin, pmax = minetest.string_to_area(area)
        if pwanted == nil or pwanted > 0xFFFF or pwanted < 0x0001 then 
            return false, "Invalid score, should be hex between 0001 and FFFF"
        end
        if pmin == nil or pmax == nil then
            return false, "Invalid area specified"
        end
        local bool, missing_privs = minetest.check_player_privs(name, {
            protection_bypass = true
        })
        if not bool then
            local pthis = sharktest.protect.user_cache[name]

            if pthis == nil then
                pthis = 0x0000
            end

            if pwanted > pthis then
                return false, "You may only protect up to your own pscore"
            end

            -- leave this expensive call last
            if sharktest.protect.is_area_protected(pmin, pmax, name, pwanted) then
                return false, "You are not allowed to override protections here"
            end
        end

        -- now for fun stuff: appending protection
        local ptable = {
            owner=name,
            score=pwanted,
            pmin=pmin,
            pmax=pmax,
        }
        table.insert(sharktest.protect.prot_cache, ptable)
        sharktest.protect.save()
        return true, "Protection with index " .. #sharktest.protect.prot_cache .. " created: " .. dump(ptable)
    end
}
minetest.register_chatcommand("protect", sharktest.protect.cmd_protect)

sharktest.protect.cmd_unprotect = {
    params = "<index>",
    description = "Unprotect area at <index>",
    privs = { st_protect = true, protection_bypass = true },
    func = function(name, param)
        local pwanted = tonumber(param)

        if pwanted == nil then
            return false, "Protection index is blank"
        end

        if pwanted > #sharktest.protect.prot_cache then
            return false, "Protection " .. pwanted .. " does not exist"
        end

        local prot = sharktest.protect.prot_cache[pwanted]
        local user = sharktest.protect.user_cache[name]

        if prot.score > user then
            return false, "You may only unprotect protections up to your pscore"
        end

        -- now for fun stuff: removing
        table.remove(sharktest.protect.prot_cache, pwanted)
        sharktest.protect.save()
        return true, "Protection with index " .. pwanted .. " removed"
    end
}

minetest.register_chatcommand("unprotect", sharktest.protect.cmd_unprotect)

sharktest.protect.cmd_pscore = {
    params = "[user] [score]",
    description = "Set/get pscore of a user",
    privs = { st_protect = true },
    func = function(name, param)
        local puser, rscore = string.match(param, "(%g+) (%x+)")
        if puser ~= nil then
            local bool, missing_privs = minetest.check_player_privs(name, {
                protection_bypass = true
            })

            if not bool then
                return false, "You are not authorized to modify users' pscores"
            end
        elseif name ~= nil then
            puser = name
        else
            return false, "You are not a player"
        end
        if rscore ~= nil then
            local pscore = tonumber(rscore, 16)
            if pscore > 0xFFFF or pscore < 0x0001 then 
                return false, "Invalid score defined, should be hex between 0001 and FFFF"
            end
            sharktest.protect.user_cache[puser] = pscore
            sharktest.protect.save()
            return true, puser .. "'s pscore is now " .. sharktest.protect.user_cache[puser]
        elseif sharktest.protect.user_cache[puser] ~= nil then
            return true, puser .. "'s pscore is " .. sharktest.protect.user_cache[puser]
        else
            return true, puser .. " does not have a pscore"
        end
    end
}
minetest.register_chatcommand("pscore", sharktest.protect.cmd_pscore)

sharktest.protect.cmd_protections = {
    params = "",
    description = "List protections here",
    privs = {},
    func = function(name, param)
        local player = minetest.get_player_by_name(name)
        if player == nil then
            return false, "You are not a player"
        end
        local pos = player:get_pos()
        local protections = {}
        for i, n in ipairs(sharktest.protect.prot_cache) do
            local area = VoxelArea:new{MinEdge=n.pmin, MaxEdge=n.pmax}
            if area:containsp(pos) then
                table.insert(protections, {id = i, data = n})
            end
        end
        return true, "This area's protections: " .. dump(protections)
    end
}
minetest.register_chatcommand("protections", sharktest.protect.cmd_protections)
---------------------------------------
-- FOOTER
---------------------------------------
sharktest.protect.load()
sharktest.print("Exiting st_protect")