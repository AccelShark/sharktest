---------------------------------------
-- Handle base stuff
---------------------------------------
sharktest = {}
sharktest.print = function(what) print("(" .. os.date("%H:%M:%S") .. ") [Sharktest] " .. what) end
sharktest.chat_send_player = function(name, text)
    sharktest.print(text)
    minetest.chat_send_player(name, "(" .. os.date("%H:%M:%S") .. ") [Sharktest] " .. text)
end
